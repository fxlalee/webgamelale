﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;


namespace Memo.ViewCounter
{
    public class HitCounter
    {
        public void AddCount(viewCounter VC)
        {
            using (MyDatebaseEntities1 db = new MyDatebaseEntities1()) 
            {
                DateTime today = DateTime.Now.Date;
                //This code is fore check unique ip per day only
                var v = db.viewCounter.Where(a => a.IPAddress.Equals(VC.IPAddress)
                && EntityFunctions.TruncateTime(a.CreateDate) == today).FirstOrDefault();
                if (v == null)
                {
                    db.viewCounter.Add(VC);
                    db.SaveChanges();
                }
            }
        }
        public object[] GetCount()
        {
            object[] o = new object[2];
            using (MyDatebaseEntities1 db = new MyDatebaseEntities1())
            {
                DateTime today = DateTime.Now.Date;
                // get Today Hits
                o[0] = db.viewCounter.Where(a => EntityFunctions.TruncateTime(a.CreateDate) == today).Count();

                // get All Hits

                o[1] = db.viewCounter.Count();



            }
            return o;
        }
    }
}