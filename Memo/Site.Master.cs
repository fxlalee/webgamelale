﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

namespace Memo
{

    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //get the ip adres for user count
            ViewCounter.HitCounter myCounter = new ViewCounter.HitCounter();

            string hostName = Dns.GetHostName();

            viewCounter VC = new viewCounter();
            VC.CreateDate = DateTime.Now;
            VC.IPAddress = Dns.GetHostByName(hostName).AddressList[0].ToString(); ;
            myCounter.AddCount(VC);

            object[] o = myCounter.GetCount();
            lblToday.Text = o[0].ToString();
            lblTotal.Text = o[1].ToString();
        }
    }
}