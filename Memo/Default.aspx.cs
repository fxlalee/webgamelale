﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Memo.ViewCounter;
namespace Memo
{


    public partial class _Default : Page
    {
        //Here we take random Images
        Random random = new Random();
        List<string> Images = new List<string>()
        {
            "1.jpg","1.jpg","2.jpg","2.jpg","3.jpg","3.jpg","4.jpg","4.jpg",
            "5.jpg","5.jpg","6.jpg","6.jpg","7.jpg","7.jpg","8.jpg","8.jpg"
        };

        protected void Page_Load(object sender, EventArgs e)
        {


            if (!this.IsPostBack)
            {

                AssignImagesToSquares();

            }

        }

        private void AssignImagesToSquares()
        {
            // asaign random images to the image button
            ImageButton MyImage;
            int randomNumber;

            for (int i = 0; i < pnlLabel.Controls.Count; i++)
            {
                if (pnlLabel.Controls[i] is ImageButton)
                    MyImage = (ImageButton)pnlLabel.Controls[i];
                else
                    continue;
                randomNumber = random.Next(0, Images.Count);
                MyImage.ImageUrl = "/img/0.jpg";
                MyImage.Attributes.Add("game", "/img/" + Images[randomNumber]);
                Images.RemoveAt(randomNumber);
            }
        }



        private bool CheckForWinner()
        {
            //check if all image is open. If so then return true else false

            bool blResult = true;
            for (int i = 0; i < pnlLabel.Controls.Count; i++)
            {
                ImageButton MyImage;
                if (pnlLabel.Controls[i] is ImageButton)
                    MyImage = (ImageButton)pnlLabel.Controls[i];
                else
                    continue;

                if (MyImage.ImageUrl == "/img/0.jpg")
                    blResult = false;


            }

            return blResult;
        }

        protected void Img1_Click(object sender, ImageClickEventArgs e)
        {
            // open clicked image and asign this to a variable
            if (firstClicked.ImageUrl != "" && secondClicked.ImageUrl != "")
            {
                if (firstClicked.ImageUrl != secondClicked.ImageUrl)
                {

                    CloseImage();
                }

                firstClicked.ImageUrl = "";
                secondClicked.ImageUrl = "";

            }
            ImageButton Img = sender as ImageButton;
            OpenImage(Img);
        }


        private void OpenImage(ImageButton img)
        {

            img.ImageUrl = img.Attributes["game"];
            if (firstClicked.ImageUrl == "")
            {
                firstClicked.ImageUrl = img.ImageUrl;
            }
            else
            {
                secondClicked.ImageUrl = img.ImageUrl;
            }

            if (CheckForWinner() == true)
            {
                Response.Write("<script language='javascript'>");
                Response.Write("alert('You Win!');");
                Response.Write("</script>");

                AssignImagesToSquares();
            }




        }

        private void CloseImage()
        {
            ImageButton MyImage;

            for (int i = 0; i < pnlLabel.Controls.Count; i++)
            {
                if (pnlLabel.Controls[i] is ImageButton)
                    MyImage = (ImageButton)pnlLabel.Controls[i];
                else
                    continue;

                if (firstClicked.ImageUrl == MyImage.ImageUrl)
                    MyImage.ImageUrl = "/img/0.jpg";

                if (secondClicked.ImageUrl == MyImage.ImageUrl)
                    MyImage.ImageUrl = "/img/0.jpg";

            }

        }




    }
}